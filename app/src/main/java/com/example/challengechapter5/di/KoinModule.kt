package com.example.challengechapter5.di

import com.example.challengechapter5.api.APIClient
import com.example.challengechapter5.database.keranjang.KeranjangDatabase
import com.example.challengechapter5.database.profile.ProfileDatabase
import com.example.challengechapter5.repository.SimpleRepository
import com.example.challengechapter5.viewmodel.SimpleViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object KoinModule {
    val dataModule
        get() = module {
            //DATABASE
            single { KeranjangDatabase.getInstance(context = get()) }
            single { ProfileDatabase.getInstance(context = get()) }
            //API
            single { APIClient.instance }
            
            //REPOSITORY
            factory { SimpleRepository(get<KeranjangDatabase>().keranjangDAO(), get<ProfileDatabase>().profileDAO()) }
        }


    val uiModule
        get() = module {
            viewModel { SimpleViewModel(get())}
        }
}