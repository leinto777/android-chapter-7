package com.example.challengechapter5.model


import com.google.gson.annotations.SerializedName

data class CategoryMenuResponse(
    @SerializedName("data")
    val `data`: List<DataCategoryMenu>,
    @SerializedName("message")
    val message: String?,
    @SerializedName("status")
    val status: Boolean?
)