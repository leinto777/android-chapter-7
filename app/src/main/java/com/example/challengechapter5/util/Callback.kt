package com.example.challengechapter5.util

import com.example.challengechapter5.database.keranjang.Keranjang

interface Callback {
    fun onKeranjangLoaded(keranjang: Keranjang?): Keranjang?
}