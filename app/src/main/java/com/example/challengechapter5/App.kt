package com.example.challengechapter5

import android.app.Application
import com.example.challengechapter5.di.KoinModule.dataModule
import com.example.challengechapter5.di.KoinModule.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@App)
            modules(
                listOf(
                    dataModule,
                    uiModule
                )
            )
        }
    }
}