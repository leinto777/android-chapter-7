package com.example.challengechapter5.view.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.challengechapter5.databinding.ActivityLoginBinding
import com.example.challengechapter5.util.AuthVerification
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        auth = Firebase.auth

        binding.buttonLogin.setOnClickListener {
            val emailVerification = AuthVerification.emailVerification(binding.etEmail.text.toString())
            val passwordVerification = AuthVerification.passwordVerification(binding.etPassword.text.toString())

            if (!emailVerification && !passwordVerification) {
                Toast.makeText(this, "Email & password tidak valid!", Toast.LENGTH_SHORT).show()
            } else if (!emailVerification) {
                Toast.makeText(this, "Email tidak valid!", Toast.LENGTH_SHORT).show()
            } else if (!passwordVerification) {
                Toast.makeText(this, "Password harus memilki 6 karakter / tidak kosong!", Toast.LENGTH_SHORT).show()
            } else {
                login(binding.etEmail.text.toString(), binding.etPassword.text.toString())
            }

        }

        binding.tvRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if (currentUser != null) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun login(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(
                        this, "Authentication failed.", Toast.LENGTH_SHORT,).show()
                }
            }
    }
}