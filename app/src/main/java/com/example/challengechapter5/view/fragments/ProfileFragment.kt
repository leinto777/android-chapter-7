package com.example.challengechapter5.view.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.challengechapter5.view.ui.LoginActivity
import com.example.challengechapter5.R
import com.example.challengechapter5.databinding.FragmentProfileBinding
import com.example.challengechapter5.viewmodel.SimpleViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.koin.android.ext.android.inject


class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth
    private val viewModel: SimpleViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        auth = Firebase.auth
        val userEmail = Firebase.auth.currentUser?.email

        readProfile(userEmail.toString())
        logout()
        toEditProfile()

        return binding.root
    }

    private fun logout() {
        binding.tvLogout.setOnClickListener {
            auth.signOut()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            startActivity(intent)
        }

        binding.logoutButton.setOnClickListener {
            auth.signOut()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun readProfile(email: String) {
        viewModel.getProfile(email).observe(viewLifecycleOwner){
            binding.tvEmailValue.text = it.email
            binding.tvMobileValue.text = it.mobile
            binding.tvAlamatValue.text = it.alamat
        }
    }

    private fun toEditProfile() {
        binding.btnEditProfile.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_updateProfileFragment)
        }
    }

}