package com.example.challengechapter5.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengechapter5.R
import com.example.challengechapter5.adapter.AdapterCategory
import com.example.challengechapter5.adapter.AdapterFood
import com.example.challengechapter5.databinding.FragmentHomeBinding
import com.example.challengechapter5.util.SharedPreferenceHelper
import com.example.challengechapter5.model.CategoryMenuResponse
import com.example.challengechapter5.model.DataListMenu
import com.example.challengechapter5.model.ListMenuResponse
import com.example.challengechapter5.util.Status
import com.example.challengechapter5.viewmodel.SimpleViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.koin.android.ext.android.inject

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SimpleViewModel by inject()

    private var isListView = true

    private lateinit var auth: FirebaseAuth

    private val layoutIcon = arrayListOf(
        R.drawable.baseline_view_list_24,
        R.drawable.baseline_grid_view_24
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        auth = Firebase.auth
        val currentUser = auth.currentUser
        if (currentUser != null) {
            "Hai ${currentUser.email},".also { binding.greetingText.text = it }
        }

        isListView = SharedPreferenceHelper.read("isListView", true)

        val toggleButton = binding.imageButton

        toggleButton.setOnClickListener {
            isListView =  !isListView
            toggleImageViewImage(toggleButton)
            SharedPreferenceHelper.write("isListView", isListView)
            fetchListMenuCoroutines()
        }

        fetchCategoryCoroutines()
        fetchListMenuCoroutines()

        return binding.root
    }

    private fun toggleImageViewImage(imageView: ImageView) {
        imageView.setImageResource(layoutIcon[if (isListView) 0 else 1])
    }

    private fun fetchCategoryCoroutines() {
        viewModel.getAllCategory().observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    showCategory(it.data)
                }
                Status.ERROR -> {
                    Log.d("Error", "Error Occured!")
                }
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun fetchListMenuCoroutines() {
        viewModel.getAllMenu().observe(viewLifecycleOwner) {
            when (it.status) {
                Status.SUCCESS -> {
                    showListMenu(it.data!!)
                    binding.progressBar.visibility = View.GONE
                }

                Status.ERROR -> {
                    Log.d("Error", "Error Occured!")
                }

                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun showCategory(data: CategoryMenuResponse?) {
        val adapter = AdapterCategory()

        adapter.submitCategoryMenuResponse(data?.data ?: emptyList())
        binding.rvCategory.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvCategory.adapter = adapter
    }

    @Suppress("KotlinConstantConditions", "KotlinConstantConditions")
    @SuppressLint("NotifyDataSetChanged")
    private fun showListMenu(data: ListMenuResponse) {
        if (isListView) {
            val adapter = AdapterFood(isListView, object : AdapterFood.OnClickListener {
                override fun onClickItem(data: DataListMenu) {
                    navigateAndSendDataToDetail(data)
                }
            })
            adapter.submitListMenuResponse(data.data)
            binding.rvFood.layoutManager = GridLayoutManager(requireActivity(), 2)
            binding.rvFood.adapter = adapter
        } else {
            val adapter = AdapterFood(isListView, object : AdapterFood.OnClickListener {
                override fun onClickItem(data: DataListMenu) {
                    navigateAndSendDataToDetail(data)
                }
            })

            adapter.submitListMenuResponse(data.data)
            binding.rvFood.layoutManager = LinearLayoutManager(requireActivity())
            binding.rvFood.adapter = adapter
        }
    }

    private fun navigateAndSendDataToDetail(data: DataListMenu) {
        val bundle = bundleOf("dataListMenu" to data)
        findNavController().navigate(R.id.action_homeFragment_to_detailMenuFragment, bundle)
    }

}