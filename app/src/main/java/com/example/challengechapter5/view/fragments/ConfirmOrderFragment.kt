package com.example.challengechapter5.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengechapter5.R
import com.example.challengechapter5.adapter.AdapterKeranjang
import com.example.challengechapter5.databinding.FragmentConfirmOrderBinding
import com.example.challengechapter5.model.ItemOrder
import com.example.challengechapter5.model.OrdersData
import com.example.challengechapter5.viewmodel.SimpleViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.koin.android.ext.android.inject

class ConfirmOrderFragment : Fragment() {
    private var _binding: FragmentConfirmOrderBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SimpleViewModel by inject()
    private lateinit var adapterKeranjang: AdapterKeranjang
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConfirmOrderBinding.inflate(inflater, container, false)

        fetchData()
        summary()

        viewModel.orderSuccess.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(requireContext(), "Success order", Toast.LENGTH_SHORT).show()
                showDialogConfirmation()
            }
        }

        binding.btnCheckout.setOnClickListener {
            auth = Firebase.auth
            val currentUser = auth.currentUser
            val orderItem = viewModel.items.value ?: emptyList()

            if (orderItem.isNotEmpty()) {
                //mapping
                val orderRequest = OrdersData(currentUser?.email.toString(), summary(), orderItem.map {
                    ItemOrder(it.name, it.quantity, it.note, it.totalPrice!!)
                })

//                keranjangViewModel.postData(orderRequest)
                viewModel.postData(orderRequest)
            } else {
                Toast.makeText(requireContext(), "Data Kosong", Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }

    private fun fetchData() {

        adapterKeranjang = AdapterKeranjang(viewModel)
        binding.rvConfirmOrder.adapter = adapterKeranjang
        binding.rvConfirmOrder.layoutManager = LinearLayoutManager(requireContext())
        viewModel.getAllItemCart().observe(viewLifecycleOwner){
            adapterKeranjang.setData(it)

            var totalPrice = 0
            it.forEach { item ->
                totalPrice += item.totalPrice!!
            }
        }
    }

    private fun summary(): Int {
        var grandTotal = 0
        viewModel.items.observe(viewLifecycleOwner) {
            var listMenu = ""
            var totalPrice = 0
            var priceMenu = ""
            it.forEach { item ->
                listMenu += "${item.name} - ${item.quantity} x ${item.basePrice}\n"
                priceMenu += "Rp. ${item.totalPrice}\n"
                totalPrice += item.totalPrice!!
            }

            val totalText = "Rp. $totalPrice"
            grandTotal = totalPrice
            binding.tvKonfirmasiHargaPesanan.nameText.text = listMenu
            binding.tvKonfirmasiHargaPesanan.quantityText.text = priceMenu
            binding.tvKonfirmasiHargaPesanan.priceText.text = totalText
        }
        return grandTotal
    }

    private fun showDialogConfirmation() {
        val dialog = DialogFragment()
        dialog.setDialogListener(object : DialogFragment.DialogListener {
            override fun onDialogPositiveClick() {
                findNavController().navigate(R.id.action_confirmOrderFragment_to_homeFragment)
            }
        })
        dialog.show(childFragmentManager, DialogFragment.TAG)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}