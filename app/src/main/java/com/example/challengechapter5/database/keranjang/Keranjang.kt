package com.example.challengechapter5.database.keranjang

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "cart")
data class Keranjang(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @ColumnInfo(name = "food_name")
    var name: String,
    @ColumnInfo(name = "food_Note")
    var note: String?,
    @ColumnInfo(name ="food_Price")
    var basePrice: Int? = 0,
    @ColumnInfo(name = "total_Price")
    var totalPrice: Int? = 0,
    @ColumnInfo(name = "food_Quantity")
    var quantity: Int = 1,
    @ColumnInfo(name = "img_Id")
    var images: String,
): Parcelable
