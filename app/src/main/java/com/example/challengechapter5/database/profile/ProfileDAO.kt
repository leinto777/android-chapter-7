package com.example.challengechapter5.database.profile

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ProfileDAO {

    @Insert
    fun insert(data: Profile)

    @Query("SELECT * FROM profile WHERE email_user = :email")
    fun getProfile(email: String): LiveData<Profile>
    
    @Update
    fun update(data: Profile)


}