package com.example.challengechapter5.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.challengechapter5.api.APIClient
import com.example.challengechapter5.api.APIService
import com.example.challengechapter5.database.keranjang.Keranjang
import com.example.challengechapter5.database.keranjang.KeranjangDAO
import com.example.challengechapter5.database.profile.Profile
import com.example.challengechapter5.database.profile.ProfileDAO
import com.example.challengechapter5.model.DataListMenu
import com.example.challengechapter5.model.OrdersData
import com.example.challengechapter5.util.Callback
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class SimpleRepository (private val keranjangDAO: KeranjangDAO, private val profileDAO: ProfileDAO) {

    private val apiService : APIService = APIClient.instance
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    // Cart
    private val _selectedItem = MutableLiveData<DataListMenu>()
    val selectedItem : LiveData<DataListMenu> = _selectedItem
    private val _counter = MutableLiveData(1)
    val counter: LiveData<Int> = _counter
    private val _orderSuccess = MutableLiveData<Boolean>()
    val orderSuccess : LiveData<Boolean> = _orderSuccess
    val items: LiveData<List<Keranjang>> = getAllCartItems()

    fun initSelectedItem(data: DataListMenu) {
        _selectedItem.value = data
    }

    fun incrementCount() {
        _counter.value = (_counter.value ?: 1) + 1
    }

    fun decrementCount() {
        val currentValue = _counter.value ?: 1
        if (currentValue > 1) {
            _counter.value = currentValue - 1
        }
    }

    fun increment(keranjang: Keranjang) {
        val newTotal = keranjang.quantity + 1
        keranjang.quantity = newTotal
        keranjang.totalPrice = keranjang.basePrice?.times(newTotal)

        updateQty(keranjang)
    }

    fun decrement(keranjang: Keranjang) {
        var newTotal = keranjang.quantity
        if (newTotal > 1) {
            newTotal = keranjang.quantity - 1
        }
        keranjang.quantity = newTotal
        keranjang.totalPrice = keranjang.basePrice?.times(newTotal)
        updateQty(keranjang)
    }

    fun getAllCartItems(): LiveData<List<Keranjang>> = keranjangDAO.getAllItems()

    private fun getItem(foodName: String, callback: Callback) {
        executorService.execute {
            val keranjang = keranjangDAO.getItemLive(foodName)
            callback.onKeranjangLoaded(keranjang)
        }
    }

    private fun update(keranjang: Keranjang) {
        keranjangDAO.update(keranjang)
    }

    fun deleteById(itemId: Int) {
        executorService.execute { keranjangDAO.deleteItemById(itemId) }
    }

    private fun insertDataKeranjang(keranjang: Keranjang) {
        executorService.execute { keranjangDAO.insert(keranjang) }
    }

    fun insertData(note: String) {
        val selectedItem = _selectedItem.value
        selectedItem?.let {
            val itemKeranjang = Keranjang(
                name = it.nama.toString(),
                note = note,
                basePrice = it.harga,
                totalPrice = it.harga!!.times(counter.value!!.toInt()),
                quantity = counter.value!!.toInt(),
                images = it.imageUrl.toString()
            )

            getItem(itemKeranjang.name, object : Callback {
                override fun onKeranjangLoaded(keranjang: Keranjang?): Keranjang? {
                    if (keranjang != null) {
                        val total = counter.value!!.toInt() + keranjang.quantity
                        keranjang.quantity = total
                        keranjang.totalPrice = keranjang.basePrice!!.times(total)
                        update(keranjang)
                    } else {
                        insertDataKeranjang(itemKeranjang)
                    }
                    return keranjang
                }
            })
        }
    }

    private fun updateQty(keranjang: Keranjang) {
        executorService.execute { keranjangDAO.update(keranjang) }
    }

    private fun deleteAll() {
        executorService.execute { keranjangDAO.delete() }
    }

    suspend fun postOrder(ordersData: OrdersData) {
        val statusOrder = apiService.postOrder(ordersData)
        if(statusOrder.status == true) {
            _orderSuccess.postValue(true)
            deleteAll()
        } else {
            _orderSuccess.postValue(false)
        }
    }

    //Profile
    fun getProfileUser(email: String): LiveData<Profile> = profileDAO.getProfile(email)
    fun updateProfileUser(profile: Profile) {
        executorService.execute { profileDAO.update(profile) }
    }

    // API
    suspend fun getCategory() = apiService.getCategoryMenu()
    suspend fun getListMenu() = apiService.getListMenu()
}