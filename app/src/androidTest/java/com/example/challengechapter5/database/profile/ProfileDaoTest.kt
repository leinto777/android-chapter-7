package com.example.challengechapter5.database.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.challengechapter5.getOrAwaitValue
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class ProfileDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: ProfileDatabase
    private lateinit var dao: ProfileDAO

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ProfileDatabase::class.java
        ).allowMainThreadQueries().build()

        dao = database.profileDAO()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun insertDataProfile() = runBlocking {
        val profileData = Profile(
            id = 1,
            alamat = "testAlamat",
            email = "someMail@gmail.com",
            mobile = "08512345678"
        )

        dao.insert(profileData)
        val getProfileByEmail = dao.getProfile("someMail@gmail.com").getOrAwaitValue()

        assertThat(getProfileByEmail).isEqualTo(profileData)
    }

    @Test
    fun getDataProfileByEmail() = runBlocking {
        val profileData = Profile(
            id = 1,
            alamat = "testAlamat",
            email = "someMail@gmail.com",
            mobile = "08512345678"
        )

        dao.insert(profileData)
        val getProfileByEmail = dao.getProfile("someMail@gmail.com").getOrAwaitValue()

        assertThat(getProfileByEmail).isEqualTo(profileData)
    }

    @Test
    fun testUpdateProfile() = runBlocking {
        val profileData = Profile(
            id = 1,
            alamat = "testAlamat",
            email = "someMail@gmail.com",
            mobile = "08512345678"
        )

        val updatedProfile = Profile(
            id = 1,
            alamat = "alamat2",
            email = "someMail@gmail.com",
            mobile = "08512345678"
        )

        dao.insert(profileData)
        dao.update(updatedProfile)
        val getProfileByEmail = dao.getProfile("someMail@gmail.com").getOrAwaitValue()

        assertThat(getProfileByEmail).isEqualTo(updatedProfile)
    }
}