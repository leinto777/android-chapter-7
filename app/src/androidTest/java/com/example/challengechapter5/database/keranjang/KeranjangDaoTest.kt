package com.example.challengechapter5.database.keranjang

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.challengechapter5.getOrAwaitValue
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class KeranjangDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: KeranjangDatabase
    private lateinit var dao: KeranjangDAO

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            KeranjangDatabase::class.java
        ).allowMainThreadQueries().build()

        dao = database.keranjangDAO()
    }

    @After
    fun teardown(){
        database.close()
    }

    @Test
    fun insertDataKeranjang() = runBlocking {
        val keranjangItem = Keranjang(
            1,
            "name",
            "note",
            0,
            0,
            1,
            "image_url"
            )
        dao.insert(keranjangItem)

        val allKeranjangItems = dao.getAllItems().getOrAwaitValue()

        assertThat(allKeranjangItems).contains(keranjangItem)
    }

    @Test
    fun deleteAllShoppingItem() = runBlocking {
        val keranjangItem = Keranjang(
            1,
            "name",
            "note",
            0,
            0,
            1,
            "image_url"
        )
        dao.insert(keranjangItem)
        dao.delete()

        val allKeranjangItems = dao.getAllItems().getOrAwaitValue()

        assertThat(allKeranjangItems).doesNotContain(keranjangItem)
    }

    @Test
    fun updateDataKeranjang() = runBlocking {
        val keranjangItem = Keranjang(
            1,
            "name",
            "note",
            0,
            0,
            1,
            "image_url"
        )

        val updatedKeranjangItem = Keranjang(
            1,
            "name",
            "note",
            1,
            1,
            1,
            "image_url"
        )
        dao.insert(keranjangItem)
        dao.update(updatedKeranjangItem)

        val allKeranjangItems = dao.getAllItems().getOrAwaitValue()

        assertThat(allKeranjangItems).contains(updatedKeranjangItem)
    }

    @Test
    fun deleteItemById() = runBlocking {
        val keranjangItem = Keranjang(
            1,
            "name",
            "note",
            0,
            0,
            1,
            "image_url"
        )
        dao.insert(keranjangItem)
        dao.deleteItemById(1)

        val allKeranjangItems = dao.getAllItems().getOrAwaitValue()

        assertThat(allKeranjangItems).isEmpty()
    }

    @Test
    fun getItemByName() = runBlocking {
        val keranjangItem = Keranjang(
            1,
            "name",
            "note",
            0,
            0,
            1,
            "image_url"
        )
        dao.insert(keranjangItem)

        val result = dao.getItemLive("name")

        assertThat(keranjangItem).isEqualTo(result)
    }

    @Test
    fun updateQuantitiyByName() = runBlocking {
        val keranjangItem = Keranjang(
            1,
            "name",
            "note",
            0,
            0,
            1,
            "image_url"
        )
        dao.insert(keranjangItem)

        val addNewQuantity = 1

        val updatedQuantity = Keranjang(
            1,
            "name",
            "note",
            0,
            0,
            1 + addNewQuantity,
            "image_url"
        )
        dao.updateQuantitiyByName(addNewQuantity, "name")

        assertThat(keranjangItem.quantity + addNewQuantity).isEqualTo(updatedQuantity.quantity)
    }

}