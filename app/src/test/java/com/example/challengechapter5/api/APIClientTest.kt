package com.example.challengechapter5.api

import com.example.challengechapter5.model.CategoryMenuResponse
import com.example.challengechapter5.model.DataCategoryMenu
import com.example.challengechapter5.model.DataListMenu
import com.example.challengechapter5.model.ItemOrder
import com.example.challengechapter5.model.ListMenuResponse
import com.example.challengechapter5.model.OrderResponse
import com.example.challengechapter5.model.OrdersData
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class APIClientTest {

    @Mock
    private lateinit var mockApiClient: APIClient

    @Mock
    private lateinit var mockApiService: APIService

    @Before
    fun setup() {
        @Suppress("DEPRECATION")
        MockitoAnnotations.initMocks(this)
        mockApiClient = mock(APIClient::class.java)
    }

    @Test
    fun testBaseUrl() {
        val baseUrl = APIClient.getUrlBase()
        assertThat(APIClient.getRetrofitBuilder().baseUrl().toUrl().toString()).isEqualTo(baseUrl)
    }

    @Test
    fun testGetListMenuStatus_isTrue() = runBlocking {
        val mockDataList = listOf(DataListMenu(
            alamatResto = "alamat",
            createdAt = null,
            detail = "detail",
            harga = 2000,
            hargaFormat = "Rp. 2000",
            id = null,
            imageUrl = null,
            nama = null,
            updatedAt = null
        ))
        val mockResponse = ListMenuResponse(mockDataList, "success", true)
        `when`(mockApiService.getListMenu()).thenReturn(mockResponse)

        val result = mockApiService.getListMenu()

        assertThat(result.status).isTrue()
    }

    @Test
    fun testGetListMenuMessage_isEqualTo_Success() = runBlocking {
        val mockDataList = listOf(DataListMenu(
            alamatResto = "alamat",
            createdAt = null,
            detail = "detail",
            harga = 2000,
            hargaFormat = "Rp. 2000",
            id = null,
            imageUrl = null,
            nama = null,
            updatedAt = null
        ))
        val mockResponse = ListMenuResponse(mockDataList, "success", true)
        `when`(mockApiService.getListMenu()).thenReturn(mockResponse)

        val result = mockApiService.getListMenu()

        assertThat(result.message).isEqualTo("success")
    }

    @Test
    fun testGetListMenuData_isNotEmpty() = runBlocking {
        val mockDataList = listOf(DataListMenu(
            alamatResto = "alamat",
            createdAt = null,
            detail = "detail",
            harga = 2000,
            hargaFormat = "Rp. 2000",
            id = null,
            imageUrl = null,
            nama = null,
            updatedAt = null
        ))
        val mockResponse = ListMenuResponse(mockDataList, "success", true)
        `when`(mockApiService.getListMenu()).thenReturn(mockResponse)

        val result = mockApiService.getListMenu()

        assertThat(result.data).isNotEmpty()
    }

    @Test
    fun testGetCategoryFoodStatus_isTrue() = runBlocking {
        val mockCategoryList = listOf(DataCategoryMenu(
            id = 1,
            imageUrl = "some.url.com",
            nama = "test",
            createdAt = "test",
            updatedAt = "test"

        ))
        val mockResponse = CategoryMenuResponse(mockCategoryList, "success", true)
        `when`(mockApiService.getCategoryMenu()).thenReturn(mockResponse)

        val result = mockApiService.getCategoryMenu()

        assertThat(result.status).isTrue()
    }

    @Test
    fun testGetCategoryMenuMessage_isEqualTo_Success() = runBlocking {
        val mockCategoryList = listOf(DataCategoryMenu(
            id = 1,
            imageUrl = "some.url.com",
            nama = "test",
            createdAt = "test",
            updatedAt = "test"

        ))
        val mockResponse = CategoryMenuResponse(mockCategoryList, "success", true)
        `when`(mockApiService.getCategoryMenu()).thenReturn(mockResponse)

        val result = mockApiService.getCategoryMenu()

        assertThat(result.message).isEqualTo("success")
    }

    @Test
    fun testGetCategoryMenuData_isNotEmpty() = runBlocking {
        val mockCategoryList = listOf(DataCategoryMenu(
            id = 1,
            imageUrl = "some.url.com",
            nama = "test",
            createdAt = "test",
            updatedAt = "test"

        ))
        val mockResponse = CategoryMenuResponse(mockCategoryList, "success", true)
        `when`(mockApiService.getCategoryMenu()).thenReturn(mockResponse)

        val result = mockApiService.getCategoryMenu()

        assertThat(result.data).isNotEmpty()
    }

    @Test
    fun testPostOrderDataSuccess() = runBlocking {

        val listItemOrder = listOf(ItemOrder(
            nama = "test_nama",
            qty = 1,
            catatan = "catatan",
            harga = 20000
        ))

        val mockOrderData = OrdersData(
            username = "test_username",
            total = 20000,
            orders = listItemOrder
        )

        val mockResponse = OrderResponse(201, "Success Create Menu!", true)
        `when`(mockApiService.postOrder(mockOrderData)).thenReturn(mockResponse)

        val result = mockApiService.postOrder(mockOrderData)

        assertThat(result.status).isTrue()
        assertThat(result.code).isEqualTo(201)
        assertThat(result.message).isEqualTo("Success Create Menu!")
    }


}