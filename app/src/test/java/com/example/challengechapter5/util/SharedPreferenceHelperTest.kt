package com.example.challengechapter5.util

import android.content.Context
import android.content.SharedPreferences
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class SharedPreferenceHelperTest {
    @Mock
    private lateinit var mockContext: Context

    @Mock
    private lateinit var mockSharedPreferences: SharedPreferences

    @Mock
    private lateinit var mockEditor: SharedPreferences.Editor

    @Before
    fun setup(){
        @Suppress("DEPRECATION")
        MockitoAnnotations.initMocks(this)
        `when`(mockContext.getSharedPreferences("layout", Context.MODE_PRIVATE)).thenReturn(mockSharedPreferences)
        `when`(mockSharedPreferences.edit()).thenReturn(mockEditor)

    }

    @Test
    fun testWrite(){
        val key = "testKey"
        val value = true

        SharedPreferenceHelper.init(mockContext)
        SharedPreferenceHelper.write(key, value)

        verify(mockEditor).putBoolean(key, value)
        verify(mockEditor).apply()
        verify(mockEditor).commit()
    }

    @Test
    fun testReadIfValueTrue_ReturnsTrue() {
        val key = "testKey"
        val defaultValue = true

        `when`(mockSharedPreferences.getBoolean(key, defaultValue)).thenReturn(true)

        SharedPreferenceHelper.init(mockContext)
        val result = SharedPreferenceHelper.read(key, defaultValue)

        verify(mockSharedPreferences).getBoolean(key, defaultValue)
        assertThat(result).isTrue()
    }
}