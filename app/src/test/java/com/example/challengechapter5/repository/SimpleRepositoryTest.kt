package com.example.challengechapter5.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.challengechapter5.api.APIClient
import com.example.challengechapter5.api.APIService
import com.example.challengechapter5.database.keranjang.KeranjangDAO
import com.example.challengechapter5.database.profile.ProfileDAO
import com.example.challengechapter5.model.DataListMenu
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.Dispatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SimpleRepositoryTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var keranjangDAO: KeranjangDAO

    @Mock
    lateinit var profileDAO: ProfileDAO

    @Mock
    lateinit var apiService: APIService

    @Mock
    lateinit var observer: Observer<Int>

    private lateinit var simpleRepository: SimpleRepository

    @Before
    fun setup() {
    @Suppress("DEPRECATION")
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(Dispatchers.Unconfined) // Set main dispatcher for testing
        simpleRepository = SimpleRepository(keranjangDAO, profileDAO)
        simpleRepository.counter.observeForever(observer)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // Reset main dispatcher after testing
    }

    @Test
    fun testInitSelectedItem() {
        val dataListMenu = DataListMenu(
            nama = "testNama",
            alamatResto = "testAlamat",
            detail = "testDetail",
            harga = 100,
            hargaFormat = "Rp 100",
            id = 1,
            imageUrl = "some.url.com",
            createdAt = "created",
            updatedAt = "updated"
        )
        simpleRepository.initSelectedItem(dataListMenu)

        verify(observer).onChanged(1)
        assertThat(dataListMenu).isEqualTo(simpleRepository.selectedItem.value)
    }

    @Test
    fun testIncrementCount() {
        simpleRepository.incrementCount()
        verify(observer).onChanged(2)

        assertThat(simpleRepository.counter.value).isEqualTo(2)
    }
    @Test
    fun testDecrementCount() {
        simpleRepository.incrementCount()
        simpleRepository.decrementCount()

        verify(observer).onChanged(2)

        assertThat(simpleRepository.counter.value).isEqualTo(1)
    }
//    @Test
//    fun testPostOrder() {
//        val ordersData = OrdersData(/* Set your data here */)
//        // Mock the response from the API service
//        `when`(apiService.postOrder(ordersData)).thenReturn(/* Mock your response here */)
//
//        simpleRepository.postOrder(ordersData)
//
//        // Verify that the orderSuccess LiveData is updated accordingly
//        verify(observer).onChanged(true)
//    }
//
//    @Test
//    fun testGetProfileUser() {
//        val email = "test@example.com"
//        // Mock the response from the profileDAO
//        `when`(profileDAO.getProfile(email)).thenReturn(/* Mock your response here */)
//
//        val profile = simpleRepository.getProfileUser(email)
//
//        // Verify that the LiveData returned by getProfileUser reflects the mocked response
//        // ...
//    }

}